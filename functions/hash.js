var crypto = require( "crypto" );

exports.handler = ( event, context, callback ) => {
    let hashes = crypto.getHashes();
    let networkResponse = {
        "isBase64Encoded": false,
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        }
    }
    let results = {};
    let input = "";
    
    if( event && event.queryStringParameters && event.queryStringParameters.input ){
        input = event.queryStringParameters.input;
    }
    
    hashes.forEach( ( hash ) => {
        results[ hash ] = crypto
            .createHash( hash )
            .update( input )
            .digest( "hex" );
    } );
    
    networkResponse.body = JSON.stringify( results );
    
    callback( null, networkResponse );
};